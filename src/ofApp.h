#pragma once

#include "ofMain.h"

#define NBOXES 8000

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		ofBoxPrimitive boxes[NBOXES];
		ofColor boxColors[NBOXES];
		ofEasyCam cam;
		ofLight light1;
		ofMaterial mat;
		
};
