#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofBackground(0);

	for (int x = 0; x < 20; x++) {
		for (int y = 0; y < 20; y++) {
			for (int z = 0; z < 20; z++) {
				int currBox = (((x * 20) + y) * 20) + z;
				boxes[currBox].setPosition(glm::vec3((x * 40)-400, (y * 40)-400, (z * 40)-400));
				boxes[currBox].set(30);
				boxColors[currBox].setHsb(255, 255, 255);
			}
		}
	}
	cam.setDistance(1300);
	ofSetSmoothLighting(true);
	light1.setDiffuseColor(ofFloatColor(.95, .95, .65));
	light1.setSpecularColor(ofFloatColor(1.f, 1.f, 1.f));
	light1.setPosition(-300, 200, 1000);

	// shininess is a value between 0 - 128, 128 being the most shiny //
	mat.setShininess(120);
	// the light highlight of the material //
	mat.setSpecularColor(ofColor(255, 255, 255, 255));

}

//--------------------------------------------------------------
void ofApp::update(){
	float myTime = ofGetSystemTimeMillis();
	for (int i = 0; i < NBOXES; i++) {
		glm::vec3 pos = boxes[i].getPosition();
		float myNoise = 60.0 * ofSignedNoise(glm::vec4(pos/400.0, myTime/8000.0));
		if (myNoise < 0) myNoise = 0;
		boxes[i].set(myNoise);
		boxColors[i].setHue(myNoise*4.0);
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	cam.begin();
	ofEnableDepthTest();
	ofEnableLighting();
	light1.enable();
	//mat.begin();

	ofRotateDeg(30, 0, 1, 0);
	for(int i = 0; i < NBOXES; i++) {
		ofSetColor(boxColors[i]);
		boxes[i].draw();
	}

	//mat.end();
	ofDisableLighting();
	ofDisableDepthTest();

	cam.end();

	cam.draw();
}